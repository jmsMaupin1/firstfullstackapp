const BASE_URL = "http://localhost:3000/api";
window.addEventListener('load', function() {
    function sendFormData(user, cb) {
        if(user.username === "" 
            || user.email === "" 
            || user.password === "" 
            || user.birthday === "") {
                alert('Please fill out the entire form')
                return;
        }
        
        fetch(`${BASE_URL}/user`, {body: JSON.stringify(user), method: "POST"})
            .then(res => {
                if (res.status === 409) alert("Username already taken")
                else if (res.status === 201) alert('Success');
                return res.json()
            })
            .then(json => cb(json))
            .catch(err => console.log(err));
    }

    let form = document.getElementById('form');

    form.addEventListener('submit', event => {
        event.preventDefault();

        let username = document.getElementById('username').value;
        let email = document.getElementById('email').value;
        let password = document.getElementById('password').value;
        let birthday = document.getElementById('birthday').value;

        sendFormData({
            username,
            email,
            password,
            birthday
        }, json => console.log(json))
    });
});