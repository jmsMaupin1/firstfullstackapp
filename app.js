const express = require('express');
const jsonBody = require('body/json');
const app = express();
const port = 3000;

let users = {};

var generateID = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

app.use('/', express.static('public'))

app.post('/api/user', (req, res) => {
    jsonBody(req, res, (err, user) => {
        if (err) throw err;
        
        if(users.hasOwnProperty(user.username)) {
            res.statusCode = 409;
            res.end(JSON.stringify({error: "username already taken"}));
        } else {
            res.statusCode = 201;
            let id = generateID();
            user.id = id;
            users[user.username] = user;
            res.end(JSON.stringify(user));
        }
    })
})

app.post('*', (req, res) => { 
    res.statusCode = 404;
    res.end('404 not found!');
})

app.get('*', (req, res) => {
    res.statusCode = 404;
    res.end('404 not found!');
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});